//The program compares two files.
//std::vector<char> MD5(const wchar_t* file1) creates file hash.
//int main(int argc, char* argv[]) compares hashes

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <Wincrypt.h>
#include<vector>
using namespace std;


#define BUFSIZE 1024
#define MD5LEN  16

using namespace std;
void print(std::vector <char> const& a)		//debug code
{
	for (int i = 0; i < a.size(); i++)
		std::cout << a.at(i);
}

//MD5 from there: https://docs.microsoft.com/ru-ru/windows/win32/seccrypto/example-c-program--creating-an-md-5-hash-from-file-content?redirectedfrom=MSDN
//DWORD MD5(const wchar_t* file1) creates hash of the file
//DWORD main() is rewritten by me as follows:

std::vector<char> MD5(const wchar_t* file1)
{
	DWORD dwStatus = 0;
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFSIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	CHAR rgbDigits[] = "0123456789abcdef";
	LPCWSTR filename = file1;

	std::vector<char> hash;		//hash container

	// Logic to check usage goes here.

	hFile = CreateFile(filename,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		dwStatus = GetLastError();
		cout << "Error opening file: " << filename << "\nError:" << dwStatus;
		//return dwStatus;
	}

	// Get handle to the crypto provider
	if (!CryptAcquireContext(&hProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT))
	{
		dwStatus = GetLastError();
		printf("CryptAcquireContext failed: %d\n", dwStatus);
		CloseHandle(hFile);
		//return dwStatus;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		dwStatus = GetLastError();
		printf("CryptAcquireContext failed: %d\n", dwStatus);
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		//return dwStatus;
	}

	while (bResult = ReadFile(hFile, rgbFile, BUFSIZE,
		&cbRead, NULL))
	{
		if (0 == cbRead)
		{
			break;
		}

		if (!CryptHashData(hHash, rgbFile, cbRead, 0))
		{
			dwStatus = GetLastError();
			printf("CryptHashData failed: %d\n", dwStatus);
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			CloseHandle(hFile);
			//return dwStatus;
		}
	}

	if (!bResult)
	{
		dwStatus = GetLastError();
		printf("ReadFile failed: %d\n", dwStatus);
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		CloseHandle(hFile);
		//return dwStatus;
	}

	cbHash = MD5LEN;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		for (DWORD i = 0; i < cbHash; i++)
		{
			hash.push_back(rgbDigits[rgbHash[i] >> 4]);
			hash.push_back(rgbDigits[rgbHash[i] & 0xf]);
		}

		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv, 0);
		CloseHandle(hFile);
	}
	else
	{
		dwStatus = GetLastError();
		printf("CryptGetHashParam failed: %d\n", dwStatus);

		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv, 0);
		CloseHandle(hFile);
	}

	return hash;
	//return dwStatus;
}

int main(int argc, char* argv[])
{
	size_t converted_chars;				//argument for calling mbstowcs_s
	const int max_pos_name = 2048;		//argument for calling mbstowcs_s

	vector<char> file1_hash;			//first file hash 
	vector<char> file2_hash;			//second file hash

	if (argc > 1)
	{
		wchar_t arg1[max_pos_name];
		wchar_t arg2[max_pos_name];
		mbstowcs_s(&converted_chars, arg1, argv[1], max_pos_name);	//To MD5 works properly, we have to convert const char* to const wchar_t*
		mbstowcs_s(&converted_chars, arg2, argv[2], max_pos_name);	//I use the idea and code from there: https://ubuntuforums.org/showthread.php?t=1579640

		file1_hash = MD5(arg1);
		file2_hash = MD5(arg2);

		if (file1_hash == file2_hash)
		{
			cout << "Files are equal!\n";
			exit(EXIT_SUCCESS);
		}
			cout << "Files are not equal!\n";
			exit(EXIT_FAILURE);
	}
	else
	{
		cout << "To compare files, pass to the program IsEqual.exe two files!";
		exit(EXIT_FAILURE);
	}
}