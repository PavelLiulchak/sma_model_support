               // Material constants for TiNi
  NOTE: there must be NO comments between a key and the values to be read 

Mf, Ms, As, Af=
220.0  333.0  274.0  370.0

Young_A=   60.0   //Young's modulus in GPa of austenite
Poisson_A=  0.3   //       Poisson's ratio of austenite
Young_M=   60.0   //Young's modulus in GPa
Poisson_M=  0.3   //Poisson's ratio          of martensite

Heat expansion of austenite= 0.0 //11.0 // in 10^-6 K^-1
Heat expansion of martensite= 0.0 //6.6 // in 10^-6 K^-1
Heat conductivity of austenite= 10.0 // in W/(m*K)
Heat conductivity of martensite= 10.0 //in W/(m*K)
Specific heat of austenite= 5.5e2     //in J/(kg*K)
Specific heat of martensite= 5.5e2    //in J/(kg*K)
Density= 6.5         // in 10^3 kg/m^3

//the parameters related with martensite nucleation
Hmin= 0.7 //in %
Hsat= 4.7 //in %
k= 0.021  //in 1/MPa
Tsig_crit= 140.0 //in MPa

//parameters below take part in the derivation of hardening function f
n1, n2, n3, n4=
0.06 0.06 0.06 0.06

//Slopes:
CA, CM=
7.8 7.3	//in MPa/K

sig_aster=360.0 //in MPa
