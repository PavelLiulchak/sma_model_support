//The program creates file TempSig.txt that contains information about dependence
//transformation temperatures from apllied to specimen external load.
//The program has been tested only for results (ttesgtf.dat file) that are calculated
//according to specific wzd file (See WZD_example folder).
//The wzd-file corresponds the situations when preloaded specimen cooling down below Mf and then heating up above Af.
//To comprehend code see draft.pdf.

#include <iostream>
#include<string>
#include <fstream>
using namespace std;

//file existence checking 
void check_file(string file)
{
	ifstream fin(file);
	if (!fin.is_open())
	{
		cout << "There is no " << file << " file!\n";
		fin.close();
		exit(1);
	}
	fin.close();
}

//to count number of columns in ttesgtf.dat file
int calc_columns(string file)
{
	int res;			//number of columns
	string buf;			//buffer string

	ifstream fin(file);
	getline(fin, buf);	//DANGER PLACE! To get row of columns names.
						//we have to guarantee that first row in ttesgtf.dat is the row of names!
	fin.close();

	res = 0;
	int i_1 = 0;
	for (int i = 0; i < (buf.length()); i++)
	{
		i_1 = i + 1;
		if (
			((buf[i] != ' ') && (buf[i_1] == ' '))||
			((buf[i] != ' ') && (buf[i_1] == '\0'))||	// \0 - terminator - symbol of row end
			((buf[i] != '\t') && (buf[i_1] == '\t'))||
			((buf[i] != '\t') && (buf[i_1] == '\0'))
			)
		{
			res++;		//counting number of spaces (' ' or tabulation symbol '\t')
		}
	}
	return res;
}

//helper function of detect_column_number()
//to count part of columns in ttesgtf.dat file;
//the part of columns is bounded by integer argument "pos";
//"pos" - position of the pattern within header (see detect_column_number())
int calc_columns(string header, int pos)
{
	int res;
	res = 0;
	int i_1 = 0;
	for (int i = 0; i < pos - 1; i++)
	{
		i_1 = i + 1;
		if (((header[i] != ' ') && (header[i_1] == ' '))||
			((header[i] != '\t') && (header[i_1] == '\t')))
		{
			res++;
		}
	}
	return res;
}

//the function returns the number of column that name is "pattern"
//the numeration of the columns begins with 0
int detect_column_number(string header, string pattern)
{
	int column = 0;
	int colmn_pos_witn_hdr = 0;
	colmn_pos_witn_hdr = header.find(pattern);
	column = calc_columns(header, colmn_pos_witn_hdr);
	return column;
}

//to set numbers of searching columns
void set_column_numb(int& s33_pos, int& e33_pos, int& s32_pos, int& g32_pos, int& tem_pos, int& e33Phase_pos, int& faza_pos, string file)
{
	ifstream fin(file);
	string buf;				//buffer string
	getline(fin, buf);		//to get row of columns names

	string s33 = "s33";							//name of column that contains normal stress values
	s33_pos = detect_column_number(buf, s33);	//number of column that contains normal stress values

	string e33 = "e33";							//name of column that contains uniaxial strain values
	e33_pos = detect_column_number(buf, e33);

	string s32 = "s32";							//name of column that contains shear stress values
	s32_pos = detect_column_number(buf, s32);

	string g32 = "g32";							//name of column that contains shear strain values
	g32_pos = detect_column_number(buf, g32);

	string tem = "tem";							//name of column that contains temperature values
	tem_pos = detect_column_number(buf, tem);

	string e33Phase = "e33Pha";					//name of column that contains uniaxial transformation strain values
	e33Phase_pos = detect_column_number(buf, e33Phase);	

	string faza = "faza";							//name of column that contains phase (martensite volume fraction) values
	faza_pos= detect_column_number(buf, faza);		//faza is transliteration of the russian word "����" that means "phase"

	fin.close();
}

int main(int argc, char** argv) //See draft.pdf
{
	string resFile = "empty";							//calculation results file name
	string outputFile = "empty";

	resFile = (argc > 1) ? argv[1]:"ttesgtf.dat";
	outputFile = (argc > 2) ? argv[2] : "TempSig.txt";
	
	check_file(resFile);								//check file existence
	double Mf, Ms, As, Af;								//transformation temperatures

	int num_of_col = calc_columns(resFile);				//number of columns in ttesgtf.dat

	double* ress = new double[num_of_col];				//initialization of an array that will contain calculated data row
	for (int i = 0; i < num_of_col; i++) ress[i] = 0.0;

	int s33_pos = 0, e33_pos = 0, s32_pos = 0, g32_pos = 0, tem_pos = 0, e33Phase_pos = 0;		
	int faza_pos = 0;																					//column number initialization
	set_column_numb(s33_pos, e33_pos, s32_pos, g32_pos, tem_pos, e33Phase_pos, faza_pos, resFile);		//set column number values

	ifstream fin(resFile);
	ofstream fout(outputFile);				//output file
	
	const double init_value = -999999.0;	//variable for initialization
	double otl = 0.0;						//debug variable
											//otl is short form "otladka" - transliteration russian word "�������" that means debug

	double phase_min = 0.0;					//the minimum value of phase (martensite volume fraction)
	double phase_max = 1.0;					//the maximum value of phase
	double epsMin = init_value;				//min value of strain at phase_min point
	double epsSat = init_value;				//max value of strain at phase_max point

	double phase_0 = 0.5;					//value of phase at midle point
	double phase_1 = 0.48;					//initialization of phase variation to derive straight line equation (to find intersection with line phase=0)
	double phase_2 = 0.52;					//initialization of phase variation to derive straight line equation (to find intersection with line phase=1)
											//see draft.pdf
	
														double T_0 = 0.0; double eps_0 = 0.0;	//temperature and strain at midle point
	/*in case increasing martensite volume fraction*/	double T_1 = 0.0; double eps_1 = 0.0;
														double T_2 = 0.0; double eps_2 = 0.0;

														double T_0_dash = 0.0;	/*in draft.pdf is T_0'*/	double eps_0_dash = 0.0;	//temperature and strain at midle point
	/*in case decreasing martensite volume fraction*/	double T_1_dash = 0.0;	/*in draft.pdf is T_1'*/	double eps_1_dash = 0.0;
														double T_2_dash = 0.0;								double eps_2_dash = 0.0;
	double err = 1e-2;			//discrepancy value. Due to phase is a double variable, it is not appropriate 
								//to use operator ==.  
	double err_dash = 1e-6;		//discrepancy value. Same reasons.

	double phase_prev = 0.0;	//previous value of phase

	double K_Ms = 0.0;			//--
	double K_Mf = 0.0;			//--
	double K_As = 0.0;			//--
	double K_Af = 0.0;			//straight lines slope ratios 

	int M_count = 0;			//counter that helps calculate Ms and Mf temperatures (see code below)
	int A_count = 0;			//counter that helps calculate As and Af temperatures

	string buf;					//buffer string
	getline(fin, buf);			//we have to read (skip) head of the ttesgtf.dat. DANGER PLACE!


	int e33_des_pos = e33Phase_pos;			//This version of the program allows us to calculate  transformation temperatures
											//based on the "transformation Strain - temperature" graph.
											//If it is desirable you may replace e33Phase_pos by e33_pos and calculate transformation
											//temperatures based on the "total strain - temperature" graph

	fout << "Sig		" << "Mf		" << "Ms		" << "As		" << "Af	\n";

	while (!fin.eof())		//ttesgtf.dat file reading
	{
		for (int i = 0; i < num_of_col; i++)
		{
			fin >> ress[i];
			otl = ress[i];
		}

		if (abs(ress[faza_pos] - phase_min) < err)
		{
			epsMin= ress[e33_des_pos];
			otl = ress[faza_pos];
		}
		if (abs(ress[faza_pos] - phase_max) < err)
		{
			epsSat= ress[e33_des_pos];
			otl = ress[faza_pos];
		}

		if (ress[faza_pos] > phase_prev)	//case of phase increasing (specimen cooling)
		{
			//find for each phases (phase_0, phase_1, phase_2) T_0, T_1, T_2 respectively;
			//T_0_dash, T_1_dash, T_2_dash initialized by init_value play role of determination flags of T_0, T_1, T_2
			otl = ress[faza_pos];
			if (abs(ress[faza_pos] - phase_1) < err)
			{
				T_1 = ress[tem_pos];	T_1_dash = init_value;
				eps_1 = ress[e33_des_pos];  eps_1_dash = init_value;
			}
			if (abs(ress[faza_pos] - phase_0) < err)
			{
				T_0 = ress[tem_pos];	T_0_dash = init_value;
				eps_0 = ress[e33_des_pos];  eps_0_dash = init_value;
			}
			if (abs(ress[faza_pos] - phase_2) < err)
			{
				T_2 = ress[tem_pos];	T_2_dash = init_value;
				eps_2 = ress[e33_des_pos];  eps_2_dash = init_value;
			}
		}
		if (ress[faza_pos] < phase_prev)	//case of phase decreasing (specimen heating)
		{
			//find for each phases (phase_0, phase_1, phase_2) T_0_dash, T_1_dash, T_2_dash respectively;
			//T_0, T_1, T_2 initialized by init_value play role of determination flags of T_0_dash, T_1_dash, T_2_dash (see code below)
			if (abs(ress[faza_pos] - phase_1) < err)
			{
				T_1_dash = ress[tem_pos];	T_1 = init_value;
				eps_1_dash = ress[e33_des_pos]; eps_1 = init_value;
			}
			if (abs(ress[faza_pos] - phase_0) < err)
			{
				T_0_dash = ress[tem_pos];	T_0 = init_value;
				eps_0_dash = ress[e33_des_pos]; eps_0 = init_value;
			}
			if (abs(ress[faza_pos] - phase_2) < err)
			{
				T_2_dash = ress[tem_pos];	T_2 = init_value;
				eps_2_dash = ress[e33_des_pos]; eps_2 = init_value;
			}
		}

		if ((abs(T_1_dash - init_value)< err_dash) && (abs(T_0_dash - init_value)<err_dash) && (abs(T_2_dash - init_value)<err_dash)&&
			(abs(ress[faza_pos] - phase_max) < err))	//We have to start calculation of Mf when epsSat is read (coincides 
														//situation when  ress[faza_pos] == phase_max)
		{
			K_Ms = (eps_1 - eps_0) / (T_1 - T_0);
			Ms = ((epsMin - eps_0) / K_Ms) + T_0;

			K_Mf = (eps_2 - eps_0) / (T_2 - T_0);
			Mf = ((epsSat - eps_0)/ K_Mf) + T_0;

			if (M_count == 0)
			{
				fout << ress[s33_pos] << " ";
				fout << Mf << " ";
				fout << Ms << " ";
				A_count = 0;
			}
			M_count++;
		}
		if ((abs(T_1 - init_value) < err_dash) && (abs(T_0 - init_value) < err_dash) && (abs(T_2 - init_value) < err_dash)&&
			(abs(ress[faza_pos] - phase_min) < err))	//We have to start calculation of Af when epsMin is read (coincides 
														//situation when  ress[faza_pos] == phase_min
		{
			K_As = (eps_2_dash - eps_0_dash) / (T_2_dash - T_0_dash);
			As = ((epsSat - eps_2_dash) / K_As) + T_0_dash;

			K_Af = (eps_1_dash - eps_0_dash) / (T_1_dash - T_0_dash);
			Af = ((epsMin - eps_0_dash) / K_Af) + T_0_dash;

			if (A_count == 0)
			{
				fout << As << " ";
				fout << Af <<endl;
				M_count = 0;
			}
			A_count++;
		}
		phase_prev = ress[faza_pos];
	}

	fout.close();
	fin.close();
	cout << "TempsSig.exe is succeeded\n";
	//system("pause");
}