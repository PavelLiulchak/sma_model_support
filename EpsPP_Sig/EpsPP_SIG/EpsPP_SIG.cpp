//The program creates files MaxStr_Sig.txt and MaxTrStr_Sig.txt.
//MaxStr_Sig.txt contains dependence of total strain magnitude accumulated during forward martensite transformation
//from external load.
//MaxTrStr_Sig.txt contains dependence of transformation strain magnitude from apllied to specimen external load.
//The program has been tested only for results (ttesgtf.dat file) that are calculated
//according to specific wzd file (See WZD_example folder).
//The wzd-file corresponds the situations when preloaded specimen cooling down below Mf and then heating up above Af.

#include <iostream>
#include<string>
#include <fstream>
using namespace std;

//file existence checking 
void check_file(string file)
{
	ifstream fin(file);
	if (!fin.is_open())
	{
		cout << "There is no " << file << " file!\n";
		fin.close();
		exit(1);
	}
	fin.close();
}

//to count number of columns in ttesgtf.dat file
int calc_columns(string file)
{
	int res;			//number of columns
	string buf;			//buffer string

	ifstream fin(file);
	getline(fin, buf);	//DANGER PLACE! To get row of columns names.
						//we have to guarantee that first row in ttesgtf.dat is the row of names!
	fin.close();

	res = 0;
	int i_1 = 0;
	for (int i = 0; i < (buf.length()); i++)
	{
		i_1 = i + 1;
		if (
			((buf[i] != ' ') && (buf[i_1] == ' ')) ||
			((buf[i] != ' ') && (buf[i_1] == '\0')) ||	// \0 - terminator - symbol of row end
			((buf[i] != '\t') && (buf[i_1] == '\t')) ||
			((buf[i] != '\t') && (buf[i_1] == '\0'))
			)
		{
			res++;		//counting number of spaces (' ' or tabulation symbol '\t')
		}
	}
	return res;
}

//helper function of detect_column_number()
//It counts part of columns in ttesgtf.dat file;
//the part of columns is bounded by integer argument "pos";
//"pos" - position of the pattern within header (see detect_column_number())
int calc_columns(string header, int pos)
{
	int res;
	res = 0;
	int i_1 = 0;
	for (int i = 0; i < pos - 1; i++)
	{
		i_1 = i + 1;
		if (((header[i] != ' ') && (header[i_1] == ' ')) ||
			((header[i] != '\t') && (header[i_1] == '\t')))
		{
			res++;
		}
	}
	return res;
}

//the function returns the number of column that name is "pattern"
//the numeration of the columns begins with 0
int detect_column_number(string header, string pattern)
{
	int column = 0;
	int colmn_pos_witn_hdr = 0;
	colmn_pos_witn_hdr = header.find(pattern);
	column = calc_columns(header, colmn_pos_witn_hdr);
	return column;
}

//to set numbers of searching columns
void set_column_numb(int& s33_pos, int& e33_pos, int& s32_pos, int& g32_pos, int& tem_pos, int& e33Phase_pos, int& faza_pos, string file)
{
	ifstream fin(file);
	string buf;				//buffer string
	getline(fin, buf);		//to get row of columns names

	string s33 = "s33";							//name of column that contains normal stress values
	s33_pos = detect_column_number(buf, s33);	//number of column that contains normal stress values

	string e33 = "e33";							//name of column that contains uniaxial strain values
	e33_pos = detect_column_number(buf, e33);

	string s32 = "s32";							//name of column that contains shear stress values
	s32_pos = detect_column_number(buf, s32);

	string g32 = "g32";							//name of column that contains shear strain values
	g32_pos = detect_column_number(buf, g32);

	string tem = "tem";							//name of column that contains temperature values
	tem_pos = detect_column_number(buf, tem);

	string e33Phase = "e33Pha";					//name of column that contains uniaxial transformation strain values
	e33Phase_pos = detect_column_number(buf, e33Phase);

	string faza = "faza";							//name of column that contains phase (martensite volume fraction) values
	faza_pos = detect_column_number(buf, faza);		//faza is transliteration of the russian word "����" that means "phase"

	fin.close();
}

//The "find_min_temper" function allows us to find min temperature in the "2tem/K" column in the ttesgtf.dat file.
//It is necessary for us to know min temperature due to total strain magnitude reaches maximum value
//at that temperature.
//The min temperature has to be < Mf. We have to guarantee that condition by setting
//appropriate wzd-file (see WZD_example folder). 
//Moreover, the temperature to which sample cools down has to be same for each stress (see example below). It is
//crucial condition for correct work of the program.

//part of wzd-file:
//					--------------------------------
//
//					[etap 4] htim = 100.0  	s33 = 120
//					[etap 5] htim = 100.0  	tem = 200 - has to be same for each stress and < Mf
//					[etap 6] htim = 100.0  	tem = 425
//
//					[etap 4] htim = 100.0  	s33 = 140
//					[etap 5] htim = 100.0  	tem = 200 - has to be same for each stress and < Mf
//					[etap 6] htim = 100.0  	tem = 425
//
//					---------------------------------

double find_min_temper(string file, int tem_pos, int num)
{
	double res;
	res = DBL_MAX;

	ifstream fin(file);
	string buf;							//buffer string
	getline(fin, buf);					// DANGER PLACE!To get row of columns names.
										//we have to guarantee that first row in ttesgtf.dat is the row of names!

	double* ress = new double[num];		//initialization of an array that contains calculated data row
	for (int i = 0; i < num; i++) ress[i] = 0.0;

	while (!fin.eof())
	{
		for (int i = 0; i < num; i++)
		{
			fin >> ress[i];
		}
		if (ress[tem_pos] < res) res = ress[tem_pos];	//to find min temperature
	}
	fin.close();
	return res;
}

/*LEGACY It is not using*/
void read_temperatures(double& Mf, double& Ms, double& As, double& Af, char* file)
{
	check_file(file);
	const char appr[] = "CNST_KEY.d";
	if (*file != *appr)
	{
		cout << "file has inappropriate name (CNST_KEY.d is ok name)\n";
		exit(1);
	}

	ifstream fin(file);
	string s;
	bool found = false;

	while (!fin.eof())
	{
		getline(fin, s);
		if (s.find("Mf") != string::npos)
		{
			if (s.find("Ms") != string::npos)
			{
				if (s.find("As") != string::npos)
				{
					if (s.find("Af") != string::npos)
					{
						found = true;
						break;
					}
					else
					{
						cout << "Check temperature order in the file! Correct order is Mf, Ms, As, Af";
						exit(1);
					}
				}
			}
		}
	}
	//��� ������ ����� ������ ������, ��������� ����������� � ������������� �������:
	fin >> Mf; fin >> Ms; fin >> As; fin >> Af;
	fin.close();
}


int main(int argc, char** argv)
{
	string resFile = "empty";							//calculation results file name
	string outputFile1 = "empty";
	string outputFile2 = "empty";

	resFile = (argc > 1) ? argv[1] : "ttesgtf.dat";
	outputFile1 = (argc > 2) ? argv[2] : "MaxTrStr_Sig.txt";
	outputFile2 = (argc > 3) ? argv[3] : "MaxStr_Sig.txt";

	check_file(resFile);							//check file existence
	
	int num_of_col = calc_columns(resFile);			//calculates number of the columns in ttesgtf.dat file

	double* ress = new double[num_of_col];			//initialization of an array that will contain calculated data row
		for (int i = 0; i < num_of_col; i++) ress[i] = 0.0;
	
	int s33_pos=0, e33_pos=0, s32_pos=0, g32_pos=0, tem_pos=0, e33Phase_pos=0;			//column number initialization
	int faza_pos = 0;																	//column number initialization

	set_column_numb(s33_pos, e33_pos, s32_pos, g32_pos, tem_pos, e33Phase_pos, faza_pos, resFile);	//set column number values	according to ttesgtf.dat file

	double temp_min = find_min_temper(resFile, tem_pos, num_of_col);

	ifstream fin(resFile);
	ofstream foutPhase(outputFile1);			//output file
	ofstream fout(outputFile2);					//--
	

	double otl = 0.0;							//debug variable
												//otl is short form "otladka" - transliteration russian word "�������" that means debug

	string buf;									//buffer string
	getline(fin, buf);							//we have to read (skip) head of the ttesgtf.dat. DANGER PLACE!

		double err_temp = 1e-2;					//discrepancy value. Due to temp is a double variable, it is not appropriate 
												//to use operator == for doubles.
		double err_sig = 5e+3;					//same reason for stress (stress in MPa)		
		double err_eps = 1e-3;					//same reason for strain

		while (!fin.eof())
		{
			for (int i = 0; i < num_of_col; i++)
			{
				fin >> ress[i];
				otl=ress[i];
			}
			if (abs(ress[tem_pos]-temp_min)<err_temp)
			{
				otl = (ress[tem_pos] - temp_min);
				if ((abs(ress[s33_pos]) > err_sig) || (abs(ress[e33_pos] > err_eps)))	
				//condition above means: "if ((ress[s33_pos] != 0.0) || (ress[e33_pos] != 0.0))"
				{
					fout << ress[s33_pos] << " " <<ress[e33_pos];
					otl = ress[s33_pos];
					otl = ress[e33_pos];
				}
				if ((abs(ress[s32_pos]) > err_sig) || (abs(ress[g32_pos] > err_eps)))
				{
					fout << ress[s32_pos] << " " << ress[g32_pos];
					otl = ress[s32_pos];
					otl = ress[g32_pos];
				}
				if ((abs(ress[s33_pos]) > err_sig) || (abs(ress[e33Phase_pos] > err_eps)))
				{
					foutPhase << ress[s33_pos] << " " << ress[e33Phase_pos];
					otl = ress[s33_pos];
					otl = ress[e33Phase_pos];
				}
				fout << endl;
				foutPhase << endl;
			}
		}

	foutPhase.close();
	fout.close();
	fin.close();
	cout << "EpsPP_SIG.exe is succeeded\n";
	//system("pause");
}